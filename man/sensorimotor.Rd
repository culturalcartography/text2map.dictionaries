% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/data.R
\docType{data}
\name{sensorimotor}
\alias{sensorimotor}
\title{Lancaster Sensorimotor Norms Dictionary for English}
\format{
A data frame with 39,707 rows and 45 variables.
}
\source{
https://doi.org/10.3758/s13428-019-01316-z
}
\usage{
sensorimotor
}
\description{
A dataset containing the sensorimotor scores for nearly
40,000 English terms across six perceptual modalities and
five action effectors obtained from Lynott et al. 2020
}
\section{Variables}{

Variables:
\itemize{
\item term. unique word (lemma)
\item auditory.mean. mean rating (0--5) of how strongly word is experienced by hearing
\item gustatoy.mean. mean rating (0--5) of how strongly word is experienced by tasting
\item haptic.mean. mean rating (0--5) of how strongly word is experienced by feeling through touch
\item interoceptive.mean. mean rating (0--5) of how strongly wordis experienced by sensations inside the body
\item olfactory.mean. mean rating (0--5) of how strongly word is experienced by smelling
\item visual.mean. mean rating (0--5) of how strongly word is experienced by seeing
\item foot_leg.mean. mean rating (0--5) of how strongly word is experienced by performing an action with the foot/leg
\item hand_arm.mean. mean rating (0--5) of how strongly the concept is experienced by performing an action with the hand/arm
\item head.mean. mean rating (0--5) of how strongly the concept is experienced by performing an action with the head excluding mouth
\item mouth.mean. mean rating (0--5) of how strongly the concept is experienced by performing an action with the mouth/throat
\item torso.mean. mean rating (0--5) of how strongly the concept is experienced by performing an action with the torso
\item auditory.sd. standard deviation of auditory strength ratings
\item gustatoy.sd. standard deviation of gustatory strength ratings
\item haptic.sd. standard deviation of haptic strength ratings
\item interoceptive.sd. standard deviation of interoceptive strength ratings
\item olfactory.sd. standard deviation of olfactory strength ratings
\item visual.sd. standard deviation of visual strength ratings
\item foot_leg.sd. standard deviation of foot action strength ratings
\item hand_arm.sd. standard deviation of hand action strength ratings
\item head.sd. standard deviation of head action strength ratings
\item mouth.sd. standard deviation of mouth action strength ratings
\item torso.sd. standard deviation of torso action strength ratings
\item max_strength.perceptual. highest strength rating across six perceptual modalities
\item minkowski3.perceptual. minkowski distance (with exponent 3) of the six-dimension vector of perceptual strength from the origin
\item exclusivity.perceptual. range of perceptual strength values divided by their sum
\item dominant.perceptual. perceptual modality with highest rating
\item max_strength.action. highest strength rating across five action effectors
\item minkowski3.action. minkowski distance (with exponent 3) of the five-dimension vector of action strength from the origin
\item exclusivity.action. range of action strength values divided by their sum
\item dominant.action. action modality with highest rating
\item max_strength.sensorimotor. highest strength rating across all 11 sensorimotor dimensions
\item minkowski3.sensorimotor. minkowski distance (with exponent 3) of the 11-dimension vector of sensorimotor strength from the origin
\item exclusivity.sensorimotor. range of sensorimotor strength values divided by their sum
\item dominant.sensorimotor. sensorimotor modality with highest rating
\item n_known.perceptual. number of perceptual raters who knew the concept well enough to rate
\item list_n.perceptual. number of perceptual raters who completed the item list featuring the word
\item percent_known.perceptual. percentage of perceptual raters who knew the concept well enough to rate
\item n_known.action. number of action raters who knew the concept well enough to rate
\item list_n.action. number of action raters who completed the item list featuring the word
\item percent_known.action. percentage of action raters who knew the concept well enough to rate
\item mean_age.perceptual. mean age of perceptual raters
\item mean_age.action. mean age of action raters
\item list_id.perceptual. ID of the item list in perceptual strength norming featuring the concept
\item list_id.action. ID of the item list in action strength norming featuring the concept
}
}

\keyword{datasets}
