# [`text2map.dictionaries`](https://culturalcartography.gitlab.io/text2map.dictionaries/): Dictionaries for Text Analysis <img src="man/figures/logo.png" align="right" height="120" />

This is an R Package with several datasets for English-language dictionaries useful for text analysis. See also [`text2map`](https://culturalcartography.gitlab.io/text2map/).

### Installation


This is primarily a dataset package and therefore we will not be sending it to CRAN. You can install the latest version from GitLab:

``` r
library(remotes)
install_gitlab("culturalcartography/text2map.dictionaries")

library(text2map.dictionaries)
```

### Dictionaries

The package currently includes [20+ dictionaries](https://culturalcartography.gitlab.io/text2map.dictionaries/reference/index.html), primarily English, containing hand-ranked and inferred word "norms," as well as frequency and rank information from various corpora or administrative data. Additionally, there are a few domain-specific dictionaries documenting rare words.


## Related Packages

There are four related packages hosted on GitLab: 

- [`text2map`](https://culturalcartography.gitlab.io/text2map): text analysis functions
- [`text2map.corpora`](https://culturalcartography.gitlab.io/text2map.corpora/): 13+ text datasets
- [`text2map.pretrained`](https://culturalcartography.gitlab.io/text2map.pretrained/): pretrained embeddings and topic models
- [`text2map.theme`](https://culturalcartography.gitlab.io/text2map.theme): changes `ggplot2` aesthetics and loads viridis color scheme as default

The above packages can be installed using the following:

```r
install.packages("text2map")

library(remotes)
install_gitlab("culturalcartography/text2map.theme")
install_gitlab("culturalcartography/text2map.corpora")
install_gitlab("culturalcartography/text2map.pretrained")
```

# Contributions and Support

We welcome new dictionaries -- especially old or rare dictionaries! If you have a dictionary you would like
to be easily available to other researchers, send us an email (maintainers [at] textmapping.com) 
or submit pull requests.

Please report any issues or bugs here: https://gitlab.com/culturalcartography/text2map.dictionaries/-/issues
